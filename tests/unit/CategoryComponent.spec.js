import { shallowMount } from '@vue/test-utils'
import CategoryComponent from '@/components/CategoryComponent.vue'

test('ejecuta el click', async () => {
    const wrapper = shallowMount(CategoryComponent, {
        propsData: {
            state: true,
        },
    })

    expect(wrapper.emitted().click).toBe(undefined)

    const button = wrapper.find('button')
    button.trigger('click')
    await wrapper.vm.$nextTick()

    expect(wrapper.emitted().chooseCategoryEvent.length).toBe(1)
})
