import { shallowMount } from "@vue/test-utils";
import StartScreen from "@/components/StartScreen.vue";

test("emit 'random' on click", async () => {
    const wrapper = shallowMount(StartScreen, {
      propsData: stateStartScreen
    });
  
    const counterbutton = wrapper.find('button')
  
    expect(wrapper.emitted()['random']).toBe(undefined)
  
    counterbutton.trigger('click')
    await wrapper.vm.$nextTick()
  
    expect(wrapper.emitted()['add'].length).toBe(1)
   
  })
  