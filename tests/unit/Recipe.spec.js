import { shallowMount } from '@vue/test-utils'
import Recipe from '@/components/Recipe.vue'

test('renders props.msg when passed', () => {
    const recipeItem = {
        strMeal: 'receta',
        strMealThumb: 'foto',
    }

    const wrapper = shallowMount(Recipe, {
        propsData: {
            recipeItem: recipeItem,
        },
    })

    expect(wrapper.find('h1').text()).toBe('soy title :receta')
    expect(wrapper.find('img').attributes("src")).toBe("foto")
})
